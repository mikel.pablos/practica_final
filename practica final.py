import time
import seeed_dht
import RPi.GPIO as GPIO 
from datetime import datetime

# fecha actual ---> 0-6 (Lunes-domingo)
dt = datetime.now()
hour = dt.hour + 1
week_day = dt.weekday()
array_dias = ["Lunes", "Martes", "Miercoles", "Jueves", "Sabado", "Domingo"]

def angle_to_percent (angle) :
    if angle > 180 or angle < 0 :
        return False

    start = 4
    end = 12.5
    ratio = (end - start)/180 #de angulo a porcentaje

    angle_as_percent = angle * ratio

    return start + angle_as_percent

GPIO.setmode(GPIO.BCM)

#Use pin 17 for PWM signal
pwm_gpio = 17
frequence = 50
GPIO.setup(pwm_gpio, GPIO.OUT)
pwm = GPIO.PWM(pwm_gpio, frequence)

#GPIO.setmode(GPIO.BCM)
    #GPIO.setup(17, GPIO.IN)
    #GPIO.setup(17, GPIO.OUT)

def main():
 
    # for DHT11/DHT22
    sensor = seeed_dht.DHT("11", 16)
    # for DHT10
    # sensor = seeed_dht.DHT("10")
 
    while True:
        dt = datetime.now()
        hour = dt.hour + 1
        humi, temp = sensor.read()
        if not humi is None:
            print('{0} - DHT{1}, humidity {2:.1f}%, temperature {3:.1f}*  {4}:{5}:{6}'.format(array_dias[week_day],sensor.dht_type, humi, temp, hour, dt.minute, dt.second ))
            if (humi<30 and week_day>0 and week_day<6 and hour > 8 and hour<20):
                print("riego activado")
                pwm.start(angle_to_percent(0))
                time.sleep(1)
            else:
                print("riego parado")
                pwm.ChangeDutyCycle(angle_to_percent(180))
                time.sleep(1)
        else:
            print('DHT{0}, humidity & temperature: {1}'.format(sensor.dht_type, temp))
        time.sleep(1)
 
 
if __name__ == '__main__':
    main()
